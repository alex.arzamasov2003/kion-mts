const express = require("express");
const cors = require("cors");
const http = require("http");
const greatings = require("./greating");
const File_access = require("./db_files/models/file_access.js");
const { initDB } = require("./db_files");
const Minio = require("minio");

const s3Client = new Minio.Client({
  endPoint: '91.185.84.135',
  port: 3711,
  useSSL: false,
  accessKey: 'BLAB9l46ezQS8gFM',
  secretKey: 'MEI8Xoa6sD17Q7NTQvMg5NYfEGtNMiGp'
});
const bucketName = "kion";

const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

console.log(greatings.date);
console.log(greatings.getMessage("Lex"));

app.get("/s3urlGET/:name", async (req, res) => {
  try {
    let name = req.params.name;
    console.log(name);
    const file = await File_access.findOne({
      where: {
        fileName: name,
      },
    });
    if (file == null) {
      res.status(229).json({ message: "Error_user_get" });
    } else {
      s3Client.presignedGetObject(
        bucketName,
        file.id,
        function (err, presignedUrl) {
          if (err) return console.log(err);
          console.log(presignedUrl);
          res.status(200).json(presignedUrl);
        }
      );
    }
  } catch (error) {
    console.log(error);
  }
});

app.get("/s3urlPUT/:name", async (req, res) => {
  try {
    let name = req.params.name;
    if (
      (await File_access.findOne({
        where: {
          fileName: name,
        },
      })) != null
    ) {
      res.status(228).json({ message: "Error_user_post" });
    } else {
      const file = await File_access.create({
        user: "",
        fileName: name,
      });
      console.log(file.id);
      s3Client.presignedPutObject(
        bucketName,
        file.id,
        function (err, presignedUrl) {
          if (err) {
            deleteFile(file.id);
            return console.log(err);
          }
          console.log(presignedUrl);
          res.status(200).json(presignedUrl);
        }
      );
    }
  } catch (error) {
    console.log(error);
  }
});

app.get("/files", async (_, res) => {
  try {
    const files = await File_access.findAll();
    res.status(200).json({ files });
  } catch (error) {
    res.status(500).json({ message: "Error_users_get" });
  }
});

app.delete("/files", async (_, res) => {
  try {
    let objectsList = [];
    let num = await File_access.count();
    await File_access.findAll({}).then((fileData) => {
      for (let i = 0; i < num; ++i) {
        console.log(fileData[i].id);
        objectsList.push(fileData[i].id)
      }
    });
    console.log(objectsList)
    await File_access.destroy({
      where: {},
    });
    console.log(objectsList)
    s3Client.removeObjects(bucketName, objectsList, function (e) {
      if (e) {
        return console.log('Unable to remove Objects ', e)
      }
      console.log('Removed the objects successfully')
    })
    res.status(200).json({ message: "Delete_completed" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Error_users_delete" });
  }
});

const deleteFile = async (filelId) => {
  try {
    await File_access.destroy({
      where: {
        id: filelId,
      },
    });
  } catch (error) {
    console.log(error);
  }
};

// app.get("/user/:email/:password", async (req, res) => {
//   try {
//     const user = await Users.findOne({
//       where: {
//         email: req.params.email,
//         password: req.params.password,
//       },
//     });
//     if (user == null) {
//       res.status(229).json({ message: "Error_user_get" });
//     } else {
//       res.status(200).json(user);
//     }
//   } catch (error) {
//     res.status(500).json({ message: "Error_user_get" });
//   }
// });

// app.post("/user", async (req, res) => {
//   try {
//     if (
//       (await Users.findOne({
//         where: {
//           email: req.body.email,
//         },
//       })) != null
//     ) {
//       res.status(228).json({ message: "Error_user_post" });
//     } else {
//       const auth = await Users.create({
//         first: req.body.first,
//         last: req.body.last,
//         email: req.body.email,
//         password: req.body.password,
//       });
//       res.status(200).json(auth);
//     }
//   } catch (error) {
//     res.status(500).json({ message: "Error_user_post" });
//   }
// });

// app.delete("/users", async (_, res) => {
//   try {
//     await Users.destroy({
//       where: {},
//     });
//     res.status(200).json({ message: "Delete_completed" });
//   } catch (error) {
//     res.status(500).json({ message: "Error_users_delete" });
//   }
// });

http.createServer(app).listen(3000, () => {
  console.log("Server works on 3000 port\n");
});

initDB();
