FROM node:16.18.1-alpine

WORKDIR /app

RUN apk add --no-cache bash

COPY package.json ./
COPY package-lock.json ./

# Установка зависимостей
RUN npm ci

# Копируем 
COPY index.js ./
COPY greating.js ./
COPY db_files ./db_files/

#Команда для запуска сервера внутри контейнера
CMD [ "npm", "run", "prod" ]