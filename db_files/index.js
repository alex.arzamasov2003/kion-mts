const { Sequelize } = require("sequelize");

//Создаем instance Sequelize
const sequelizeInstance = new Sequelize({
    dialect: "postgres",
    host: "91.185.84.135",
    port: 3005,
    database: "postgres",
    username: "postgres",
    password: "T10Ij5^29uGBT@Kiq%fFcy!J",

  storage: "./sqliteData/db_file.sqlite", //Путь до файла с данными
});

const initDB = async () => {
  try {
    await sequelizeInstance.authenticate(); //Авторизация нашей ORM в БД
    // await sequelize.dropSchema('public', {});
    // await sequelize.createSchema('public', {});
    await sequelizeInstance.sync(); //Синхронизация МОДЕЛЕЙ
    console.log("Sequelize was initialized");
  } catch (error) {
    console.log("Sequelize ERROR (initDB)", error);
    process.exit();
  }
};

module.exports = {
  sequelizeInstance,
  initDB,
};