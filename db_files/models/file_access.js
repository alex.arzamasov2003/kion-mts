const Sequelize = require("sequelize");
const { sequelizeInstance } = require("..");

class File_access extends Sequelize.Model {}

File_access.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      primaryKey: true,
      //autoIncrement: true,
      defaultValue: Sequelize.DataTypes.UUIDV4,
    },
    user: {
      type: Sequelize.STRING,
      defaultValue: "",
    },
    fileName: {
      type: Sequelize.STRING,
      defaultValue: "",
    },
    isCompleted: {
      //is_completed
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
  },

  { sequelize: sequelizeInstance, underscored: true, modelName: "file_access" }
);

module.exports = File_access;