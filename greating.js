let currentDate = new Date();
module.exports.date = String("\n" + currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds());
 
module.exports.getMessage = function(name){
    let hour = currentDate.getHours();
    if(hour > 18)
        return "Добрый вечер, " + name;
    else if(hour > 12)
        return "Добрый день, " + name;
    else if(hour < 6)
        return "Доброй ночи, " + name;
    else
        return "Доброе утро, " + name;
}